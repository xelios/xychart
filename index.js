
/**
 * Dependencies
 */

/**
 * Expose `apply`
 */

module.exports = function apply($el, options) {
  $el.data('xychart', new XYChart($el, options));
};

function XYChart($container, options) {
  options = options || {};

  this.$container = $container;

  var self = this;

  this.settings = $.extend(true, {
    chart: {
      margin: {
        top: 20,
        right: 20,
        bottom: 20,
        left: 20,
      },
      width: 'auto',
      height: 'auto',
    },
    legend: {
      show: true,
      container: null,
    },
    xAxis: {
      show: true
    },
    yAxis: {
      show: true,
      normalize: true
    },
    point: {
      marker: true
    },
    data: [],
    dateFormats: {
      short: {
        hour: "%I:00%p",
        day: "%m/%d/%Y",
        week: "%m/%d",
        month: "%m/%Y",
        year: "%Y"
      },
      long: {
        hour: "%m/%d/%Y %I:00%p",
        day: "%A, %B %e, %Y",
        week: "Week of %B %e, %Y",
        month: "%B %Y",
        year: "%Y"
      }
    },
    events: {
      pointMouseOver: function(context, point) {},
      pointMouseOut: function(context, point) {},
      pointClick: function(context, point) {},
      areaMouseOver: function(context, point) {},
      areaMouseOut: function(context, point) {},
      areaClick: function(context, point) {},
      lineMouseOver: function(context, point) {},
      lineMouseOut: function(context, point) {},
      lineClick: function(context, point) {},
      barMouseOver: function(context, point) {},
      barMouseOut: function(context, point) {},
      barClick: function(context, point) {}
    }
  }, options);

  this.width = this.settings.chart.width;
  this.height = this.settings.chart.height;
  this.data = this.settings.data;
  
  if (this.width == 'auto') {
    this.width = $container.width() - 
      (this.settings.chart.margin.left + this.settings.chart.margin.right);
  }

  if (this.height == 'auto') {
    this.height = $container.height() - 
      (this.settings.chart.margin.top + this.settings.chart.margin.bottom);
  }

  this.$container.html('');

  this.svg = d3.select(this.$container.get(0))
    .append("svg")
    .attr("width", this.width + (this.settings.chart.margin.left + this.settings.chart.margin.right))
    .attr("height", this.height + (this.settings.chart.margin.top + this.settings.chart.margin.bottom))
    .append("g")
    .attr("transform", "translate(" + this.settings.chart.margin.left + "," + this.settings.chart.margin.top + ")");

  if (!this.settings.legend.container && this.settings.legend.show) {
    this.settings.legend.container = $('<div class="viewer-chart-legend"></div>').appendTo($container).html('');
  }
  else if (this.settings.legend.container && this.settings.legend.show) {
    this.settings.legend.container.html('');
  }

  this.colors = d3.scale.category10();

  var drawn_xaxis = false;
  var drawn_yaxis_left = false;
  var drawn_yaxis_right = false;

  //
  // internal structures
  //
  self.series = [];

  self.xAxis = null;
  self.yAxisLeft = null;
  self.yAxisLeftField = null;
  self.yAxisRight = null;
  self.yAxisRightField = null;

  self.xScale = {};
  self.yScale = {};

  //
  // must get max Y over all series
  //
  var y_max = {};

  for (var i in this.data) {
    var series = $.extend(true, {
      hidden: false,
      label: null,
      xAxis: {
        name: null,
        field: null
      },
      yAxis: {
        name: null,
        field: null
      }
    }, this.data[i]);

    self.series.push(series);
  }

  var y_field_index = {};
  
  $.each(this.series, function(i, seri) {
    var data = seri.data;

    var xAxis = null;

    var x_x0 = Math.ceil(0.7 * (self.width / ((self.data[0].data.length + 2) * window.REPORTER_DATERANGES.length))) * window.REPORTER_DATERANGES.length;
    var x_x1 = self.width - (x_x0 * 2);

    var x = null;
    var y = null;
    var x_steps = data.length;

    if (typeof(y_field_index[self.series[i].field]) == "undefined") {
      y_field_index[self.series[i].field] = 0;
    }
    
    if (seri.xAxis.field in self.xScale) {
      x = self.xScale[seri.xAxis.field];
    }
    else {
      if (seri.xAxis.type == "datetime") {
        x = d3.time.scale()
            .range([x_x0, x_x1])
            .domain([new Date(seri.xAxis.min), new Date(seri.xAxis.max)]);

        if (seri.xAxis.granularity == 'hour') {
          x_steps = Math.ceil((seri.xAxis.max - seri.xAxis.min) / (3600 * 1000.0));
        }
        else if (seri.xAxis.granularity == 'day') {
          x_steps = Math.ceil((seri.xAxis.max - seri.xAxis.min) / (86400 * 1000.0));
        }

        self.xScale[seri.xAxis.field] = x;
      }
      else if (seri.xAxis.type == "number") {
        x = d3.scale.linear().range([x_x0, x_x1]);
        x.domain(d3.extent(data, function(d) { return d[0]; }));
        self.xScale[seri.xAxis.field] = x;
      }
    }

    if (seri.yAxis.field in self.yScale) {
      y = self.yScale[seri.yAxis.field];
    }
    else {
      y = d3.scale.linear().range([self.height, 0]);
      y.domain([0, self.getFieldMax(seri.yAxis.field)]);
      self.yScale[seri.yAxis.field] = y;
    }
    
    if (!self.xAxis && self.settings.xAxis.show) {
      self.xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .tickFormat(d3.time.format(self.settings.dateFormats.short[seri.xAxis.granularity]));

      self.svg.append("g")
        .attr("class", "x viewer-chart-axis")
        .attr("transform", "translate(0," + self.height + ")")
        .call(self.xAxis);
    }
    
    if (!self.yAxisLeft && self.settings.yAxis.show) {
      self.yAxisLeft = d3.svg.axis()
        .scale(self.yScale[seri.yAxis.field])
        .orient("left");
      self.yAxisLeftField = self.series[i].yAxis.field;
      
      self.svg.append("g")
        .attr("class", "y viewer-chart-axis left")
        .call(self.yAxisLeft)
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", ".71em")
          .style("text-anchor", "end")
          .style("fill", self.colors(i))
          .style("font-weight", "bold")
          .text(seri.yAxis.name);
    }
    
    if (!self.yAxisRight && i > 0 && self.settings.yAxis.show) {
      self.yAxisRight = d3.svg.axis().scale(self.yScale[seri.yAxis.field]).orient("right");
      self.yAxisRightField = self.series[i].yAxis.field;
      
      self.svg.append("g")
        .attr("class", "y viewer-chart-axis right")
        .attr("transform", "translate(" + (self.width - 40) + ", 0)")
        .call(self.yAxisRight)
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", -16)
          .attr("dy", ".71em")
          .style("text-anchor", "end")
          .style("fill", self.colors(i))
          .style("font-weight", "bold")
          .text(seri.yAxis.name);
    }
    
    self.series[i].color = seri.color = d3.rgb(self.colors(i));
    self.series[i].container = seri.container = self.svg.append("g").attr("class", "viewer-chart-series");

    if (self.settings.legend.show) {
      var $legend_link = $('<a href="#" class="btn btn-link btn-sm"><i class="glyphicon glyphicon-minus"></i> <strong>' + seri.label + '</strong> ' + seri.yAxis.name + '</a>');
      $legend_link.css('color', seri.color.toString());
      $legend_link.click(function(event) {
        event.preventDefault();
        var $this = $(this);
        if ($this.hasClass('series-hidden')) {
          $this.removeClass('series-hidden');

          self.series[i].container.attr("visibility", "visible");
          self.series[i].hidden = false;
          self.redraw();
        }
        else {
          console.log("Adding class?");
          $this.addClass('series-hidden');

          self.series[i].container.attr("visibility", "hidden");
          self.series[i].hidden = true;
          self.redraw();
        }
      });
      $legend_link.appendTo(self.settings.legend.container);
    }

    $.each(window.REPORTER_DATERANGES, function(j, daterange) {
      var xindex = (j * 2);
      var yindex = (j * 2) + 1;

      if (self.series[i].yAxis.field == "posts") {
        var bar_class = "viewer-chart-bar" + (j > 0 ? ' history' : '');
        var bar_count = x_steps * (window.REPORTER_DATERANGES.length + self.getSeriesCountForField(self.series[i].yAxis.field));
        var bar_width = Math.ceil(0.7 * (self.width / bar_count));
        var bar_group_width = bar_width * window.REPORTER_DATERANGES.length;
        var bar_offset = ((bar_width * j) + (bar_width * y_field_index[self.series[i].yAxis.field])) - Math.ceil(bar_group_width / 2);

        seri.container.append("g")
          .attr("class", "rect-group-" + j)
          .selectAll("rect")
            .data(data)
            .enter().append("rect")
              .attr("class", bar_class)
              .style("fill", seri.color.brighter().toString())
              .attr("x", function(d) { return x(new Date(d[0])) + bar_offset; })
              .attr("width", bar_width)
              .attr("y", function(d) { return y(d[yindex]); })
              .attr("height", function(d) { return self.height - y(d[yindex]); })
              .on("mouseover", function(d, i) {
                var point = {
                  x: d[xindex],
                  y: d[yindex]
                };

                var coord = {
                  x: d[0],
                  y: d[yindex]
                };

                var context = {
                  xychart: self,
                  element: this,
                  series: seri,
                  scale: {
                    y: y,
                    x: x
                  }
                };

                self.settings.events.barMouseOver(context, point, coord);
              })
              .on("mouseout", function(d, i) {
                var point = {
                  x: d[xindex],
                  y: d[yindex]
                };

                var coord = {
                  x: d[0],
                  y: d[yindex]
                };

                var context = {
                  xychart: self,
                  element: this,
                  series: seri,
                  scale: {
                    y: y,
                    x: x
                  }
                };

                self.settings.events.barMouseOut(context, point, coord);
              })
              .on("click", function(d, i) {
                var point = {
                  x: d[xindex],
                  y: d[yindex]
                };

                var coord = {
                  x: d[0],
                  y: d[yindex]
                };

                var context = {
                  xychart: self,
                  element: this,
                  series: seri,
                  scale: {
                    y: y,
                    x: x
                  }
                };

                self.settings.events.barClick(context, point, coord);
              });
      }
      else if (seri.chart_type == "area") {
        var area = d3.svg.area()
          .x(function(d) { return (x(new Date(d[0]))); })
          .y0(self.height)
          .y1(function(d) { return y(d[yindex]); });
  
        var line = d3.svg.line()
          .x(function(d) { return x(new Date(d[0])); })
          .y(function(d) { return y(d[yindex]); });

        var c = seri.container.append("g").attr('class', 'path-group-' + j);
  
        c.append("path")
          .datum(data)
          .style("fill", seri.color.toString())
          .attr("class", "viewer-chart-area" + (j > 0 ? ' history' : ''))
          .attr("d", area)
          .on("mouseover", function(d, i) {
            var point = {
              x: d[xindex],
              y: d[yindex]
            };

            var coord = {
              x: d[0],
              y: d[yindex]
            };

            var context = {
              xychart: self,
              element: this,
              series: seri,
              scale: {
                y: y,
                x: x
              }
            };

            self.settings.events.areaMouseOver(context, point, coord);
          });
          
        c.append("g").attr('class', 'path-group-' + j)
          .append("path")
            .datum(data)
            .style("stroke", seri.color.toString())
            .attr("class", "viewer-chart-line" + (j > 0 ? ' history' : ''))
            .attr("d", line)
            .on("mouseover", function(d, i) {
              var point = {
                x: d[xindex],
                y: d[yindex]
              };

              var coord = {
                x: d[0],
                y: d[yindex]
              };

              var context = {
                xychart: self,
                element: this,
                series: seri,
                scale: {
                  y: y,
                  x: x
                }
              };

              self.settings.events.lineMouseOver(context, point, coord);
            });

        if (seri.xAxis.type == "datetime") {
          var dateFormat = d3.time.format(self.settings.dateFormats.long[seri.xAxis.granularity]);
          var dateFormatURL = d3.time.format("%m/%d/%Y");
          
          if (data.length < 60 && self.settings.point.marker)  {
            seri.container.append("g")
              .attr("class", "circle-group-" + j)
              .selectAll("circle")
                .data(data)
                .enter().append("circle")
                  .attr("r", 5)
                  .attr("cx", function(d) { return x(new Date(d[0])); })
                  .attr("cy", function(d) { return y(d[yindex]); })
                  .style("fill", seri.color.toString())
                  .attr("class", "viewer-chart-dot")
                  .on("mouseover", function(d) {
                    var point = {
                      x: d[xindex],
                      y: d[yindex]
                    };

                    var coord = {
                      x: d[xindex],
                      y: d[yindex]
                    };

                    var context = {
                      xychart: self,
                      element: this,
                      series: seri,
                      scale: {
                        y: y,
                        x: x
                      }
                    };

                    self.settings.events.pointMouseOver(context, point, coord);
                  })
                  .on("mouseout", function(d) {
                    var point = {
                      x: d[xindex],
                      y: d[yindex]
                    };

                    var coord = {
                      x: d[xindex],
                      y: d[yindex]
                    };

                    var context = {
                      xychart: self,
                      element: this,
                      series: seri,
                      scale: {
                        y: y,
                        x: x
                      }
                    };
                    
                    self.settings.events.pointMouseOut(context, point, coord);
                  })
                  .on("click", function(d) {
                    var point = {
                      x: d[xindex],
                      y: d[yindex]
                    };

                    var coord = {
                      x: d[xindex],
                      y: d[yindex]
                    };

                    var context = {
                      xychart: self,
                      element: this,
                      series: seri,
                      scale: {
                        y: y,
                        x: x
                      }
                    };
                    
                    self.settings.events.pointClick(context, point, coord);
                  });
          }
        }
      }
    });

    y_field_index[self.series[i].field]++;
  });
};

XYChart.prototype.getFieldMax = function(field) {
  var self = this;
  var x = 0;

  for (var i in self.series) {
    var r = self.series[i];

    if (r.yAxis.field == field && !r.hidden) {
      x = Math.max(x, r.yAxis.max);
    }
  }

  return x;
};

XYChart.prototype.getSeriesCountForField = function(field) {
  var self = this;
  var x = 0;

  for (var i in self.series) {
    if (self.series[i].field == field && !self.series[i].hidden) {
      x++;
    }
  }

  return x;
};

XYChart.prototype.redraw = function() {
  var self = this;
  var tran = self.svg.transition().duration(250);

  for (var f in self.xScale) {
    var x_x0 = Math.ceil(0.7 * (self.width / ((self.series[0].data.length + 2) * window.REPORTER_DATERANGES.length))) * window.REPORTER_DATERANGES.length;
    var x_x1 = self.width - (x_x0 * 2);
    self.xScale[f].range([x_x0, x_x1]);
  }

  for (var f in self.yScale) {
    self.yScale[f].range([self.height, 0]);
    self.yScale[f].domain([0, self.getFieldMax(f)]);
    console.log("New Max for", f, "is", self.getFieldMax(f));
  }

  tran.select(".x.viewer-chart-axis")
    .attr("transform", "translate(0," + self.height + ")")
    .call(self.xAxis);

  if (self.yAxisLeft) {
    self.svg.select(".y.viewer-chart-axis.left")
      .call(self.yAxisLeft);
  }

  if (self.yAxisRight) {
    self.svg.select('.y.viewer-chart-axis.right')
      .call(self.yAxisRight);
  }

  var y_field_index = {};

  $.each(self.series, function(i, series) {
    if (!series.hidden) {
      if (typeof(y_field_index[self.series[i].yAxis.field]) == "undefined") {
        y_field_index[self.series[i].yAxis.field] = 0;
      }

      $.each(window.REPORTER_DATERANGES, function(j, daterange) {
        var xindex = (2 * j);
        var yindex = (2 * j) + 1;

        console.log("Updating circles:", series.container.select('g.circle-group-' + j));

        if (self.series[i].yAxis.field == "posts") {
          var x_steps = self.series[i].data.length;

          if (self.series[i].xAxis.granularity == 'hour') {
            x_steps = Math.ceil((self.series[i].xAxis.max - self.series[i].xAxis.min) / (3600 * 1000.0));
          }
          else if (self.series[i].xAxis.granularity == 'day') {
            x_steps = Math.ceil((self.series[i].xAxis.max - self.series[i].xAxis.min) / (86400 * 1000.0));
          }

          var bar_class = "viewer-chart-bar" + (j > 0 ? ' history' : '');
          var bar_count = x_steps * (window.REPORTER_DATERANGES.length + self.getSeriesCountForField(self.series[i].yAxis.field));
          var bar_width = Math.ceil(0.7 * (self.width / bar_count));
          var bar_group_width = bar_width * window.REPORTER_DATERANGES.length;
          var bar_offset = ((bar_width * j) + (bar_width * y_field_index[self.series[i].yAxis.field])) - Math.ceil(bar_group_width / 2);

          self.series[i].container.select("g.rect-group-" + j)
            .selectAll("rect")
              .attr("x", function(d) { return self.xScale[series.xAxis.field](new Date(d[0])) + bar_offset; })
              .attr("width", bar_width)
              .attr("y", function(d) { return self.yScale[series.yAxis.field](d[yindex]); })
              .attr("height", function(d) { return self.height - self.yScale[series.yAxis.field](d[yindex]); });
        }
        else if (self.series[i].chart_type == "area") {
          series.container.select('g.circle-group-' + j).selectAll("circle")
            .attr("cy", function(d) { return self.yScale[series.yAxis.field](d[yindex]); });

          var area = d3.svg.area()
            .x(function(d) { return self.xScale[series.xAxis.field](new Date(d[0])); })
            .y0(self.height)
            .y1(function(d) { return self.yScale[series.yAxis.field](d[yindex]); });
    
          var line = d3.svg.line()
            .x(function(d) { return self.xScale[series.xAxis.field](new Date(d[0])); })
            .y(function(d) { return self.yScale[series.yAxis.field](d[yindex]); });

          series.container.select('g.path-group-' + j + ' path.viewer-chart-area').attr("d", area);
          series.container.select('g.path-group-' + j + ' path.viewer-chart-line').attr("d", line);
        }
      });

      y_field_index[self.series[i].yAxis.field]++;
    }
  });
};